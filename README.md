OWL Carousel Formatter
===

Field formatter for image and media element using the excellent [OWLCarousel](http://owlgraphic.com/owlcarousel/) jQuery plugin.
Loosely based on galleryformatter.

Installation
====
Install this module, and then copy the [owl-carousel](http://owlgraphic.com/owlcarousel/) files to libraries/owl-carousel
http://owlgraphic.com/owlcarousel/

Issues and feature requests
===
+ Wire up settings with http://owlgraphic.com/owlcarousel/#customizing
+ Allow for non-image media elements
+ html5 video formatter (borrow from file entity?) https://drupal.org/node/1748952
